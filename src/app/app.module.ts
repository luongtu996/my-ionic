import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { firebaseConfig } from '../config';

import { Ionic2RatingModule } from 'ionic2-rating';
import { RatingPage } from '../pages/rating/rating';
import { FormReviewPage } from '../pages/form-review/form-review';
import { FormReviewComponent } from '../components/form-review/form-review';
import { TestDatetimePage } from '../pages/test-datetime/test-datetime';
import { PopupModalPage } from '../pages/popup-modal/popup-modal';
import { ModalReviewComponent } from '../components/modal-review/modal-review';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    RatingPage,
    FormReviewPage,
    TestDatetimePage,
    PopupModalPage,

    //component
    FormReviewComponent,
    ModalReviewComponent,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),

    Ionic2RatingModule,
    AngularFireModule.initializeApp(firebaseConfig),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    RatingPage,
    FormReviewPage,
    TestDatetimePage,
    PopupModalPage,

    //component
    FormReviewComponent,
    ModalReviewComponent,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AngularFireAuth
  ]
})
export class AppModule {}
