import { Component } from '@angular/core';

/**
 * Generated class for the ModalReviewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'modal-review',
  templateUrl: 'modal-review.html'
})
export class ModalReviewComponent {

  content: string = "";
  rate: any = 3;
  constructor() {
    console.log('Hello ModalReviewComponent Component');
  }

  isEmpty(str) {
    return str.replace(/^\s+|\s+$/g, '').length < 3;
  }

}
