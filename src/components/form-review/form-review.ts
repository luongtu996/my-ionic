import { Component } from '@angular/core';

/**
 * Generated class for the FormReviewComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'form-review',
  templateUrl: 'form-review.html'
})
export class FormReviewComponent {

  text: string;

  constructor() {
    console.log('Hello FormReviewComponent Component');
    this.text = 'Hello World';
  }

}
