import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormReviewPage } from './form-review';

@NgModule({
  declarations: [
    FormReviewPage,
  ],
  imports: [
    IonicPageModule.forChild(FormReviewPage),
  ],
})
export class FormReviewPageModule {}
