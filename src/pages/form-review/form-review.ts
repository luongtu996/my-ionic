import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, AlertController } from 'ionic-angular';
import { FormReviewComponent } from '../../components/form-review/form-review';

/**
 * Generated class for the FormReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-review',
  templateUrl: 'form-review.html',
})
export class FormReviewPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormReviewPage');
  }

  presentPopover() {
    const popover = this.popoverCtrl.create(FormReviewComponent, { cssClass: 'custom-width-popover-75' });
    popover.present();

  }

  doPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Your Review',
      inputs: [
        {
          name: 'reviewContent',
          placeholder: 'your review'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'OK',
          handler: data => {
            console.log(data);
            console.log(this.isEmpty(data.reviewContent));
            
            if (data.reviewContent && !this.isEmpty(data.reviewContent)) {
              console.log("ok");
            } else {
              console.log("not Ok");
            }
          }
        }
      ],
      cssClass: 'alertCustomCss'
    });

    alert.present();
  }

  isEmpty(str) {
    return str.replace(/^\s+|\s+$/g, '').length == 0;
  }

}
