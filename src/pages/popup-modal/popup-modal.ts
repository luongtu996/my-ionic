import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Modal, ModalController } from 'ionic-angular';
import { ModalReviewComponent } from '../../components/modal-review/modal-review';

/**
 * Generated class for the PopupModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popup-modal',
  templateUrl: 'popup-modal.html',
})
export class PopupModalPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopupModalPage');
  }

  presentReviewPopup() {
    const modal: Modal = this.modalCtrl.create(ModalReviewComponent, {}, { cssClass: "modal-review" });
    modal.present();
  }

}
