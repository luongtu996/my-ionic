import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the TestDatetimePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-test-datetime',
  templateUrl: 'test-datetime.html',
})
export class TestDatetimePage {

  date: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.date = new Date();
    console.log(this.date);
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TestDatetimePage');
  }

}
