import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestDatetimePage } from './test-datetime';

@NgModule({
  declarations: [
    TestDatetimePage,
  ],
  imports: [
    IonicPageModule.forChild(TestDatetimePage),
  ],
})
export class TestDatetimePageModule {}
