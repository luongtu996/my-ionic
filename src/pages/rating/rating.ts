import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RatingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rating',
  templateUrl: 'rating.html',
})
export class RatingPage {

  rate: any = 0;
  rate1: any = 1;
  rate2: any = 2;
  constructor(public navCtrl: NavController) {
  }
  onModelChange(event) {
    this.rate = event;
    console.log(event);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RatingPage');
  }

}
